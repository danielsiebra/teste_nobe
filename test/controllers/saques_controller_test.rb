require 'test_helper'

class SaquesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @saque = saques(:one)
  end

  test "should get index" do
    get saques_url
    assert_response :success
  end

  test "should get new" do
    get new_saque_url
    assert_response :success
  end

  test "should create saque" do
    assert_difference('Saque.count') do
      post saques_url, params: { saque: { conta_id: @saque.conta_id, valor_saque: @saque.valor_saque } }
    end

    assert_redirected_to saque_url(Saque.last)
  end

  test "should show saque" do
    get saque_url(@saque)
    assert_response :success
  end

  test "should get edit" do
    get edit_saque_url(@saque)
    assert_response :success
  end

  test "should update saque" do
    patch saque_url(@saque), params: { saque: { conta_id: @saque.conta_id, valor_saque: @saque.valor_saque } }
    assert_redirected_to saque_url(@saque)
  end

  test "should destroy saque" do
    assert_difference('Saque.count', -1) do
      delete saque_url(@saque)
    end

    assert_redirected_to saques_url
  end
end
