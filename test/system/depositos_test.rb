require "application_system_test_case"

class DepositosTest < ApplicationSystemTestCase
  setup do
    @deposito = depositos(:one)
  end

  test "visiting the index" do
    visit depositos_url
    assert_selector "h1", text: "Depositos"
  end

  test "creating a Deposito" do
    visit depositos_url
    click_on "New Deposito"

    fill_in "Conta", with: @deposito.conta_id
    fill_in "Tipo deposito", with: @deposito.tipo_deposito
    fill_in "Valor deposito", with: @deposito.valor_deposito
    click_on "Create Deposito"

    assert_text "Deposito was successfully created"
    click_on "Back"
  end

  test "updating a Deposito" do
    visit depositos_url
    click_on "Edit", match: :first

    fill_in "Conta", with: @deposito.conta_id
    fill_in "Tipo deposito", with: @deposito.tipo_deposito
    fill_in "Valor deposito", with: @deposito.valor_deposito
    click_on "Update Deposito"

    assert_text "Deposito was successfully updated"
    click_on "Back"
  end

  test "destroying a Deposito" do
    visit depositos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Deposito was successfully destroyed"
  end
end
