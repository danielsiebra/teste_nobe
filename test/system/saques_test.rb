require "application_system_test_case"

class SaquesTest < ApplicationSystemTestCase
  setup do
    @saque = saques(:one)
  end

  test "visiting the index" do
    visit saques_url
    assert_selector "h1", text: "Saques"
  end

  test "creating a Saque" do
    visit saques_url
    click_on "New Saque"

    fill_in "Conta", with: @saque.conta_id
    fill_in "Valor saque", with: @saque.valor_saque
    click_on "Create Saque"

    assert_text "Saque was successfully created"
    click_on "Back"
  end

  test "updating a Saque" do
    visit saques_url
    click_on "Edit", match: :first

    fill_in "Conta", with: @saque.conta_id
    fill_in "Valor saque", with: @saque.valor_saque
    click_on "Update Saque"

    assert_text "Saque was successfully updated"
    click_on "Back"
  end

  test "destroying a Saque" do
    visit saques_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Saque was successfully destroyed"
  end
end
