require "application_system_test_case"

class ContasTest < ApplicationSystemTestCase
  setup do
    @conta = contas(:one)
  end

  test "visiting the index" do
    visit contas_url
    assert_selector "h1", text: "Contas"
  end

  test "creating a Conta" do
    visit contas_url
    click_on "New Conta"

    fill_in "Agencia", with: @conta.agencia
    check "Ativo" if @conta.ativo
    fill_in "Digito agencia", with: @conta.digito_agencia
    fill_in "Digito conta", with: @conta.digito_conta
    fill_in "Empresa", with: @conta.empresa_id
    fill_in "Numero conta", with: @conta.numero_conta
    fill_in "Pessoa", with: @conta.pessoa_id
    fill_in "Saldo", with: @conta.saldo
    fill_in "Senha", with: @conta.senha
    click_on "Create Conta"

    assert_text "Conta was successfully created"
    click_on "Back"
  end

  test "updating a Conta" do
    visit contas_url
    click_on "Edit", match: :first

    fill_in "Agencia", with: @conta.agencia
    check "Ativo" if @conta.ativo
    fill_in "Digito agencia", with: @conta.digito_agencia
    fill_in "Digito conta", with: @conta.digito_conta
    fill_in "Empresa", with: @conta.empresa_id
    fill_in "Numero conta", with: @conta.numero_conta
    fill_in "Pessoa", with: @conta.pessoa_id
    fill_in "Saldo", with: @conta.saldo
    fill_in "Senha", with: @conta.senha
    click_on "Update Conta"

    assert_text "Conta was successfully updated"
    click_on "Back"
  end

  test "destroying a Conta" do
    visit contas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Conta was successfully destroyed"
  end
end
