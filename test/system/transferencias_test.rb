require "application_system_test_case"

class TransferenciasTest < ApplicationSystemTestCase
  setup do
    @transferencia = transferencias(:one)
  end

  test "visiting the index" do
    visit transferencias_url
    assert_selector "h1", text: "Transferencias"
  end

  test "creating a Transferencia" do
    visit transferencias_url
    click_on "New Transferencia"

    check "Ativo" if @transferencia.ativo
    fill_in "Conta destino", with: @transferencia.conta_destino
    fill_in "Conta origem", with: @transferencia.conta_origem
    fill_in "Data transferencia", with: @transferencia.data_transferencia
    fill_in "Empresa", with: @transferencia.empresa_id
    fill_in "Taxa transferencia", with: @transferencia.taxa_transferencia
    fill_in "Tipo transferencia", with: @transferencia.tipo_transferencia
    fill_in "Valor transferencia", with: @transferencia.valor_transferencia
    click_on "Create Transferencia"

    assert_text "Transferencia was successfully created"
    click_on "Back"
  end

  test "updating a Transferencia" do
    visit transferencias_url
    click_on "Edit", match: :first

    check "Ativo" if @transferencia.ativo
    fill_in "Conta destino", with: @transferencia.conta_destino
    fill_in "Conta origem", with: @transferencia.conta_origem
    fill_in "Data transferencia", with: @transferencia.data_transferencia
    fill_in "Empresa", with: @transferencia.empresa_id
    fill_in "Taxa transferencia", with: @transferencia.taxa_transferencia
    fill_in "Tipo transferencia", with: @transferencia.tipo_transferencia
    fill_in "Valor transferencia", with: @transferencia.valor_transferencia
    click_on "Update Transferencia"

    assert_text "Transferencia was successfully updated"
    click_on "Back"
  end

  test "destroying a Transferencia" do
    visit transferencias_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Transferencia was successfully destroyed"
  end
end
