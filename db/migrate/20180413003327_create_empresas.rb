class CreateEmpresas < ActiveRecord::Migration[5.2]
  def change
    create_table :empresas do |t|
      t.text :nome
      t.text :razao_social
      t.string :cnpj
      t.string :telefone
      t.text :email
      t.string :cep
      t.text :endereco
      t.string :numero
      t.string :complemento
      t.boolean :ativo, default: true

      t.timestamps null: false
    end
  end
end
