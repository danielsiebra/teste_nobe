class CreateSaques < ActiveRecord::Migration[5.2]
  def change
    create_table :saques do |t|
      t.references :conta, foreign_key: true
      t.decimal :valor_saque

      t.timestamps
    end
  end
end
