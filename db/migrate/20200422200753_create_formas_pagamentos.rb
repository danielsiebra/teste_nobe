class CreateFormasPagamentos < ActiveRecord::Migration[5.2]
  def change
    create_table :formas_pagamentos do |t|
      t.string :nome
      t.attachment :imagem_pagamento
      t.boolean :ativo
      t.references :empresa, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
