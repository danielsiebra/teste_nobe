class CreateDepositos < ActiveRecord::Migration[5.2]
  def change
    create_table :depositos do |t|
      t.references :conta, foreign_key: true
      t.decimal :valor_deposito
      t.string :tipo_deposito

      t.timestamps
    end
  end
end
