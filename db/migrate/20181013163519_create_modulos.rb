class CreateModulos < ActiveRecord::Migration[5.2]
  def change
    create_table :modulos do |t|
      t.string :descricao

      t.timestamps null: false
    end
  end
end
