class CreateAutorizacaoPagamentos < ActiveRecord::Migration[5.2]
  def change
    create_table :autorizacao_pagamentos do |t|
      t.datetime :data_autorizacao
      t.boolean :ativo, default: true
      t.references :empresa, index: true, foreign_key: true
      t.references :usuario, index: true, foreign_key: true
      t.text :descricao

      t.timestamps null: false
    end
  end
end
