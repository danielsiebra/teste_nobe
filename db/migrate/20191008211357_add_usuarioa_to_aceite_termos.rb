class AddUsuarioaToAceiteTermos < ActiveRecord::Migration[5.2]
  def change
    add_reference :aceite_termos, :usuario, index: true, foreign_key: true
  end
end
