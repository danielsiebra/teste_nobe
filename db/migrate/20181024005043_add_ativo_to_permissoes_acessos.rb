class AddAtivoToPermissoesAcessos < ActiveRecord::Migration[5.2]
  def change
    add_column :permissoes_acessos, :ativo, :boolean
    add_reference :permissoes_acessos, :empresa, index: true, foreign_key: true
  end
end
