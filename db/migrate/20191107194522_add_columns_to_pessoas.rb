class AddColumnsToPessoas < ActiveRecord::Migration[5.2]
  def change
    add_column :pessoas, :cep, :string
    add_column :pessoas, :telefone, :string
    add_column :pessoas, :endereco, :string
    add_column :pessoas, :cidade, :string
    add_column :pessoas, :bairro, :string
    add_column :pessoas, :numero_imovel, :string
    add_column :pessoas, :complemento, :string
  end
end