class CreatePermissoesAcessos < ActiveRecord::Migration[5.2]
  def change
    create_table :permissoes_acessos do |t|
      t.references :usuario, index: true, foreign_key: true
      t.references :modulo, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
