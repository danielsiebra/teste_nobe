class RenameColumnsToPessoas < ActiveRecord::Migration[5.2]
  def change
    rename_column :pessoas, :endereco, :rua
    rename_column :pessoas, :numero_imovel, :numero
  end
end
