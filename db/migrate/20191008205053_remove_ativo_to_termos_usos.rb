class RemoveAtivoToTermosUsos < ActiveRecord::Migration[5.2]
  def change
    remove_column :termos_usos, :ativo, :boolean
  end
end
