class AddEmpresaeAtivoToSaques < ActiveRecord::Migration[5.2]
  def change
    add_column :saques, :ativo, :boolean
    add_reference :saques, :empresa, foreign_key: true
  end
end
