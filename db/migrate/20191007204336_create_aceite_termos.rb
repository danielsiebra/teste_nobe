class CreateAceiteTermos < ActiveRecord::Migration[5.2]
  def change
    create_table :aceite_termos do |t|
      t.references :pessoa, index: true, foreign_key: true
      t.references :termos_uso, index: true, foreign_key: true
      t.datetime :data_aceite
      t.boolean :ativo, default: true
      t.integer :empresa_id

      t.timestamps null: false
    end
  end
end
