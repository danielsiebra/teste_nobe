class CreateContas < ActiveRecord::Migration[5.2]
  def change
    create_table :contas do |t|
      t.integer :agencia
      t.integer :digito_agencia
      t.integer :numero_conta
      t.integer :digito_conta
      t.references :pessoa, foreign_key: true
      t.decimal :saldo
      t.boolean :ativo
      t.references :empresa, foreign_key: true
      t.string :senha

      t.timestamps
    end
  end
end
