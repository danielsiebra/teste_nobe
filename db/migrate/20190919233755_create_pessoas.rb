class CreatePessoas < ActiveRecord::Migration[5.2]
  def change
    create_table :pessoas do |t|
      t.string :nome
      t.string :sobrenome
      t.date :data_nascimento
      t.string :genero
      t.string :cpf
      t.string :rg
      t.references :usuario, index: true, foreign_key: true
      t.boolean :ativo, default: true
      t.integer :empresa_id

      t.timestamps null: false
    end
  end
end
