class RemovePessoaToAceiteTermos < ActiveRecord::Migration[5.2]
  def change
    remove_reference :aceite_termos, :pessoa, index: true, foreign_key: true
  end
end
