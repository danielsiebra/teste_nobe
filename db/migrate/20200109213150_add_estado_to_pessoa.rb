class AddEstadoToPessoa < ActiveRecord::Migration[5.2]
  def change
    add_column :pessoas, :estado, :string
  end
end
