class CreateEnderecosClientes < ActiveRecord::Migration[5.2]
  def change
    create_table :enderecos_clientes do |t|
      t.references :cliente, index: true, foreign_key: true
      t.references :empresa, index: true, foreign_key: true
      t.boolean :ativo
      t.string :cep
      t.string :estado
      t.string :cidade
      t.string :bairro
      t.string :rua
      t.string :numero
      t.string :complemento

      t.timestamps null: false
    end
  end
end
