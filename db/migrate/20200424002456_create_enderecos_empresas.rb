class CreateEnderecosEmpresas < ActiveRecord::Migration[5.2]
  def change
    create_table :enderecos_empresas do |t|
      t.references :filiais_empresa, index: true, foreign_key: true
      t.references :empresa, index: true, foreign_key: true
      t.boolean :ativo
      t.string :cep
      t.string :estado
      t.string :cidade
      t.string :bairro
      t.string :rua
      t.string :numero
      t.string :complemento

      t.timestamps null: false
    end
  end
end
