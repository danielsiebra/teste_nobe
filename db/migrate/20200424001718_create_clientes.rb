class CreateClientes < ActiveRecord::Migration[5.2]
  def change
    create_table :clientes do |t|
      t.string :nome
      t.string :sobrenome
      t.string :email
      t.string :contato
      t.string :razao_social
      t.date :nascimento
      t.string :tipo_pessoa
      t.string :cpf
      t.string :cnpj
      t.references :empresa, index: true, foreign_key: true
      t.boolean :ativo

      t.timestamps null: false
    end
  end
end
