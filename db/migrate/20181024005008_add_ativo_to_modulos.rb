class AddAtivoToModulos < ActiveRecord::Migration[5.2]
  def change
    add_column :modulos, :ativo, :boolean
    add_reference :modulos, :empresa, index: true, foreign_key: true
  end
end
