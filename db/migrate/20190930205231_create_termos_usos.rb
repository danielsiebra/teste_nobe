class CreateTermosUsos < ActiveRecord::Migration[5.2]
  def change
    create_table :termos_usos do |t|
      t.date :inicio_virgencia
      t.date :termino_virgencia
      t.text :termos
      t.boolean :ativo
      t.boolean :empresa_id

      t.timestamps null: false
    end
  end
end
