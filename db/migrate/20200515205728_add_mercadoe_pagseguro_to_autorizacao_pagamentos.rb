class AddMercadoePagseguroToAutorizacaoPagamentos < ActiveRecord::Migration[5.2]
  def change
    add_column :autorizacao_pagamentos, :pagseguro, :boolean
    add_column :autorizacao_pagamentos, :mercado_pago, :boolean
  end
end
