class CreateFiliaisEmpresas < ActiveRecord::Migration[5.2]
  def change
    create_table :filiais_empresas do |t|
      t.references :empresa, index: true, foreign_key: true
      t.boolean :ativo
      t.string :nome
      t.string :email
      t.string :razao_social
      t.date :abertura
      t.string :cnpj
      t.string :contato

      t.timestamps null: false
    end
  end
end
