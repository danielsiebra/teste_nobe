class AddEmpresaeAtivoToDepositos < ActiveRecord::Migration[5.2]
  def change
    add_column :depositos, :ativo, :boolean
    add_reference :depositos, :empresa, foreign_key: true
  end
end
