class AddAtivoToTermosUsos < ActiveRecord::Migration[5.2]
  def change
    add_column :termos_usos, :ativo, :boolean, default: true
  end
end
