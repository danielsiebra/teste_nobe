class CreateTransferencias < ActiveRecord::Migration[5.2]
  def change
    create_table :transferencias do |t|
      t.integer :conta_origem
      t.integer :conta_destino
      t.decimal :valor_transferencia
      t.date :data_transferencia
      t.decimal :taxa_transferencia
      t.string :tipo_transferencia
      t.references :empresa, foreign_key: true
      t.boolean :ativo

      t.timestamps
    end
  end
end
