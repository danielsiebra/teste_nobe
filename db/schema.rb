# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_10_23_011528) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "aceite_termos", force: :cascade do |t|
    t.bigint "termos_uso_id"
    t.datetime "data_aceite"
    t.boolean "ativo", default: true
    t.integer "empresa_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "usuario_id"
    t.index ["termos_uso_id"], name: "index_aceite_termos_on_termos_uso_id"
    t.index ["usuario_id"], name: "index_aceite_termos_on_usuario_id"
  end

  create_table "autorizacao_pagamentos", force: :cascade do |t|
    t.datetime "data_autorizacao"
    t.boolean "ativo", default: true
    t.bigint "empresa_id"
    t.bigint "usuario_id"
    t.text "descricao"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "pagseguro"
    t.boolean "mercado_pago"
    t.index ["empresa_id"], name: "index_autorizacao_pagamentos_on_empresa_id"
    t.index ["usuario_id"], name: "index_autorizacao_pagamentos_on_usuario_id"
  end

  create_table "clientes", force: :cascade do |t|
    t.string "nome"
    t.string "sobrenome"
    t.string "email"
    t.string "contato"
    t.string "razao_social"
    t.date "nascimento"
    t.string "tipo_pessoa"
    t.string "cpf"
    t.string "cnpj"
    t.bigint "empresa_id"
    t.boolean "ativo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "pessoa_id"
    t.text "observacao"
    t.index ["empresa_id"], name: "index_clientes_on_empresa_id"
    t.index ["pessoa_id"], name: "index_clientes_on_pessoa_id"
  end

  create_table "contas", force: :cascade do |t|
    t.integer "agencia"
    t.integer "digito_agencia"
    t.integer "numero_conta"
    t.integer "digito_conta"
    t.bigint "pessoa_id"
    t.decimal "saldo"
    t.boolean "ativo"
    t.bigint "empresa_id"
    t.string "senha"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["empresa_id"], name: "index_contas_on_empresa_id"
    t.index ["pessoa_id"], name: "index_contas_on_pessoa_id"
  end

  create_table "depositos", force: :cascade do |t|
    t.bigint "conta_id"
    t.decimal "valor_deposito"
    t.string "tipo_deposito"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "ativo"
    t.bigint "empresa_id"
    t.index ["conta_id"], name: "index_depositos_on_conta_id"
    t.index ["empresa_id"], name: "index_depositos_on_empresa_id"
  end

  create_table "empresas", force: :cascade do |t|
    t.text "nome"
    t.text "razao_social"
    t.string "cnpj"
    t.string "telefone"
    t.text "email"
    t.string "cep"
    t.text "endereco"
    t.string "numero"
    t.string "complemento"
    t.boolean "ativo", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "emprestimos", force: :cascade do |t|
    t.bigint "cliente_id"
    t.bigint "empresa_id"
    t.boolean "ativo"
    t.decimal "valor_emprestimo"
    t.date "data_emprestimo"
    t.date "data_vencimento"
    t.string "prazo"
    t.decimal "juros"
    t.decimal "valor_parcela"
    t.decimal "valor_total_a_pagar"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cliente_id"], name: "index_emprestimos_on_cliente_id"
    t.index ["empresa_id"], name: "index_emprestimos_on_empresa_id"
  end

  create_table "enderecos_clientes", force: :cascade do |t|
    t.bigint "cliente_id"
    t.bigint "empresa_id"
    t.boolean "ativo"
    t.string "cep"
    t.string "estado"
    t.string "cidade"
    t.string "bairro"
    t.string "rua"
    t.string "numero"
    t.string "complemento"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cliente_id"], name: "index_enderecos_clientes_on_cliente_id"
    t.index ["empresa_id"], name: "index_enderecos_clientes_on_empresa_id"
  end

  create_table "enderecos_empresas", force: :cascade do |t|
    t.bigint "filiais_empresa_id"
    t.bigint "empresa_id"
    t.boolean "ativo"
    t.string "cep"
    t.string "estado"
    t.string "cidade"
    t.string "bairro"
    t.string "rua"
    t.string "numero"
    t.string "complemento"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["empresa_id"], name: "index_enderecos_empresas_on_empresa_id"
    t.index ["filiais_empresa_id"], name: "index_enderecos_empresas_on_filiais_empresa_id"
  end

  create_table "filiais_empresas", force: :cascade do |t|
    t.bigint "empresa_id"
    t.boolean "ativo"
    t.string "nome"
    t.string "email"
    t.string "razao_social"
    t.date "abertura"
    t.string "cnpj"
    t.string "contato"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["empresa_id"], name: "index_filiais_empresas_on_empresa_id"
  end

  create_table "formas_pagamentos", force: :cascade do |t|
    t.string "nome"
    t.string "imagem_pagamento_file_name"
    t.string "imagem_pagamento_content_type"
    t.bigint "imagem_pagamento_file_size"
    t.datetime "imagem_pagamento_updated_at"
    t.boolean "ativo"
    t.bigint "empresa_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["empresa_id"], name: "index_formas_pagamentos_on_empresa_id"
  end

  create_table "modulos", force: :cascade do |t|
    t.string "descricao"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "ativo"
    t.bigint "empresa_id"
    t.index ["empresa_id"], name: "index_modulos_on_empresa_id"
  end

  create_table "parcelas_emprestimos", force: :cascade do |t|
    t.bigint "empresa_id"
    t.boolean "ativo"
    t.bigint "emprestimo_id"
    t.decimal "valor_parcela"
    t.date "date"
    t.date "data_pago"
    t.decimal "valor_pago"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["empresa_id"], name: "index_parcelas_emprestimos_on_empresa_id"
    t.index ["emprestimo_id"], name: "index_parcelas_emprestimos_on_emprestimo_id"
  end

  create_table "permissoes_acessos", force: :cascade do |t|
    t.bigint "usuario_id"
    t.bigint "modulo_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "ativo"
    t.bigint "empresa_id"
    t.index ["empresa_id"], name: "index_permissoes_acessos_on_empresa_id"
    t.index ["modulo_id"], name: "index_permissoes_acessos_on_modulo_id"
    t.index ["usuario_id"], name: "index_permissoes_acessos_on_usuario_id"
  end

  create_table "pessoas", force: :cascade do |t|
    t.string "nome"
    t.string "sobrenome"
    t.date "data_nascimento"
    t.string "genero"
    t.string "cpf"
    t.string "rg"
    t.bigint "usuario_id"
    t.boolean "ativo", default: true
    t.integer "empresa_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "cep"
    t.string "telefone"
    t.string "rua"
    t.string "cidade"
    t.string "bairro"
    t.string "numero"
    t.string "complemento"
    t.string "estado"
    t.string "tipo_pessoa"
    t.index ["usuario_id"], name: "index_pessoas_on_usuario_id"
  end

  create_table "saques", force: :cascade do |t|
    t.bigint "conta_id"
    t.decimal "valor_saque"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "ativo"
    t.bigint "empresa_id"
    t.index ["conta_id"], name: "index_saques_on_conta_id"
    t.index ["empresa_id"], name: "index_saques_on_empresa_id"
  end

  create_table "termos_usos", force: :cascade do |t|
    t.date "inicio_virgencia"
    t.date "termino_virgencia"
    t.text "termos"
    t.boolean "empresa_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "ativo", default: true
  end

  create_table "transferencias", force: :cascade do |t|
    t.integer "conta_origem"
    t.integer "conta_destino"
    t.decimal "valor_transferencia"
    t.date "data_transferencia"
    t.decimal "taxa_transferencia"
    t.string "tipo_transferencia"
    t.bigint "empresa_id"
    t.boolean "ativo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["empresa_id"], name: "index_transferencias_on_empresa_id"
  end

  create_table "usuarios", force: :cascade do |t|
    t.string "nome"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_usuarios_on_email", unique: true
    t.index ["reset_password_token"], name: "index_usuarios_on_reset_password_token", unique: true
  end

  add_foreign_key "aceite_termos", "termos_usos"
  add_foreign_key "aceite_termos", "usuarios"
  add_foreign_key "autorizacao_pagamentos", "empresas"
  add_foreign_key "autorizacao_pagamentos", "usuarios"
  add_foreign_key "clientes", "empresas"
  add_foreign_key "clientes", "pessoas"
  add_foreign_key "contas", "empresas"
  add_foreign_key "contas", "pessoas"
  add_foreign_key "depositos", "contas"
  add_foreign_key "depositos", "empresas"
  add_foreign_key "emprestimos", "clientes"
  add_foreign_key "emprestimos", "empresas"
  add_foreign_key "enderecos_clientes", "clientes"
  add_foreign_key "enderecos_clientes", "empresas"
  add_foreign_key "enderecos_empresas", "empresas"
  add_foreign_key "enderecos_empresas", "filiais_empresas"
  add_foreign_key "filiais_empresas", "empresas"
  add_foreign_key "formas_pagamentos", "empresas"
  add_foreign_key "modulos", "empresas"
  add_foreign_key "parcelas_emprestimos", "empresas"
  add_foreign_key "parcelas_emprestimos", "emprestimos"
  add_foreign_key "permissoes_acessos", "empresas"
  add_foreign_key "permissoes_acessos", "modulos"
  add_foreign_key "permissoes_acessos", "usuarios"
  add_foreign_key "pessoas", "usuarios"
  add_foreign_key "saques", "contas"
  add_foreign_key "saques", "empresas"
  add_foreign_key "transferencias", "empresas"
end
