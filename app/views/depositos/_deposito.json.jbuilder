json.extract! deposito, :id, :conta_id, :valor_deposito, :tipo_deposito, :created_at, :updated_at
json.url deposito_url(deposito, format: :json)
