json.array!(@termos_usos) do |termos_uso|
  json.extract! termos_uso, :id, :inicio_virgencia, :termino_virgencia, :termos, :ativo, :empresa_id
  json.url termos_uso_url(termos_uso, format: :json)
end
