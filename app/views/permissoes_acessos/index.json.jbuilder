json.array!(@permissoes_acessos) do |permissoes_acesso|
  json.extract! permissoes_acesso, :id, :usuario_id, :modulo_id
  json.url permissoes_acesso_url(permissoes_acesso, format: :json)
end
