json.array!(@modulos) do |modulo|
  json.extract! modulo, :id, :descricao
  json.url modulo_url(modulo, format: :json)
end
