json.extract! saque, :id, :conta_id, :valor_saque, :created_at, :updated_at
json.url saque_url(saque, format: :json)
