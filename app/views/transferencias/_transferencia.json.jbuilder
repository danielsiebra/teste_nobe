json.extract! transferencia, :id, :conta_origem, :conta_destino, :valor_transferencia, :data_transferencia, :taxa_transferencia, :tipo_transferencia, :empresa_id, :ativo, :created_at, :updated_at
json.url transferencia_url(transferencia, format: :json)
