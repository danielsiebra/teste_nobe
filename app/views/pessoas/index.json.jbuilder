json.array!(@pessoas) do |pessoa|
  json.extract! pessoa, :id, :nome, :sobrenome, :data_nascimento, :genero, :cpf, :rg, :usuario_id, :ativo, :empresa_id
  json.url pessoa_url(pessoa, format: :json)
end
