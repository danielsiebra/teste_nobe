json.array!(@empresas) do |empresa|
  json.extract! empresa, :id, :nome, :razao_social, :cnpj, :telefone, :email, :cep, :endereco, :numero, :complemento, :ativo
  json.url empresa_url(empresa, format: :json)
end
