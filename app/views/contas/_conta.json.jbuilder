json.extract! conta, :id, :agencia, :digito_agencia, :numero_conta, :digito_conta, :pessoa_id, :saldo, :ativo, :empresa_id, :senha, :created_at, :updated_at
json.url conta_url(conta, format: :json)
