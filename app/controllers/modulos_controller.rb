class ModulosController < ApplicationController
  before_action :set_modulo, only: [:show, :edit, :update, :destroy]

  # GET /modulos
  # GET /modulos.json
  def index
    @modulos = Modulo.where(:ativo => true, :empresa_id => session[:empresa_id]).order(:descricao)
  end

  # GET /modulos/1
  # GET /modulos/1.json
  def show
  end

  # GET /modulos/new
  def new
    @modulo = Modulo.new
  end

  # GET /modulos/1/edit
  def edit
  end

  # POST /modulos
  # POST /modulos.json
  def create
    @modulo = Modulo.new(modulo_params)
    @modulo.ativo = true
    @modulo.empresa_id = session[:empresa_id]

    respond_to do |format|
      if @modulo.save
        format.html { redirect_to @modulo, notice: 'Módulo cadastrado com sucesso.' }
        format.json { render action: 'show', status: :created, location: @modulo }
      else
        format.html { render action: 'new' }
        format.json { render json: @modulo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /modulos/1
  # PATCH/PUT /modulos/1.json
  def update
    respond_to do |format|
      if @modulo.update(modulo_params)
        @modulo.empresa_id = session[:empresa_id]
        format.html { redirect_to @modulo, notice: 'Módulo atualizado com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @modulo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /modulos/1
  # DELETE /modulos/1.json
  def destroy
    @modulo.update(ativo: :false)
    respond_to do |format|
      format.html { redirect_to modulos_url, notice: 'Módulo deletado com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_modulo
      @modulo = Modulo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def modulo_params
      params.require(:modulo).permit(:descricao, :ativo, :empresa_id)
    end
end
