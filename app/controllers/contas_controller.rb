class ContasController < ApplicationController
  before_action :set_conta, only: [:show, :edit, :update, :destroy]

  # GET /contas
  # GET /contas.json
  def index
    @contas = Conta.where(ativo: true, empresa_id: session[:empresa_id])
  end

  # GET /contas/1
  # GET /contas/1.json
  def show
  end

  # GET /contas/new
  def new
    @conta = Conta.new
  end

  # GET /contas/1/edit
  def edit
  end

  # POST /contas
  # POST /contas.json
  def create
    @conta = Conta.new(conta_params)
    @conta.agencia = 0001
    @conta.digito_agencia = 1
    @conta.saldo = 0
    @conta.ativo = true
    @conta.empresa_id = session[:empresa_id]

    respond_to do |format|
      @conta.numero_conta = "#{1010}#{@conta.id}".to_i
      @conta.digito_conta = 1
      if @conta.save
        format.html { redirect_to @conta, notice: 'Cadastrado com sucesso.' }
        format.json { render :show, status: :created, location: @conta }
      else
        format.html { render :new }
        format.json { render json: @conta.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contas/1
  # PATCH/PUT /contas/1.json
  def update
    respond_to do |format|
      if @conta.update(conta_params)
        format.html { redirect_to @conta, notice: 'Atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @conta }
      else
        format.html { render :edit }
        format.json { render json: @conta.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contas/1
  # DELETE /contas/1.json
  def destroy
    @conta.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to contas_url, notice: 'Deletado com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_conta
      @conta = Conta.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def conta_params
      params.require(:conta).permit(:agencia, :digito_agencia, :numero_conta, :digito_conta, :pessoa_id, :saldo, :ativo, :empresa_id, :senha)
    end
end
