class DashboardsController < ApplicationController

  def dashboard_1
    @data_inicial = Time.now.beginning_of_month
    @data_final = Time.now.end_of_month
    @depositos = Deposito.where(ativo: true, empresa_id: session[:empresa_id], created_at: [@data_inicial..@data_final]).sum(:valor_deposito)
    @transferencias = Transferencia.where(ativo: true, empresa_id: session[:empresa_id], created_at: [@data_inicial..@data_final]).sum(:valor_transferencia)
    @taxas_transferencias = Transferencia.where(ativo: true, empresa_id: session[:empresa_id], created_at: [@data_inicial..@data_final]).sum(:taxa_transferencia)
    @saques = Saque.where(ativo: true, empresa_id: session[:empresa_id], created_at: [@data_inicial..@data_final]).sum(:valor_saque)
    
    depositos_totais = Deposito.where(ativo: true, empresa_id: session[:empresa_id]).sum(:valor_deposito)
    taxas_transferencias_totais = Transferencia.where(ativo: true, empresa_id: session[:empresa_id]).sum(:taxa_transferencia)
    transferencias_totais = Transferencia.where(ativo: true, empresa_id: session[:empresa_id]).sum(:valor_transferencia)
    saques_totais = Saque.where(ativo: true, empresa_id: session[:empresa_id]).sum(:valor_saque)
    @saldo = depositos_totais - transferencias_totais - saques_totais - taxas_transferencias_totais
  end

end
