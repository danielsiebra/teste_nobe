class PermissoesAcessosController < ApplicationController
  before_action :set_permissoes_acesso, only: [:show, :edit, :update, :destroy]

  # GET /permissoes_acessos
  # GET /permissoes_acessos.json
  def index
    @permissoes_acessos = PermissoesAcesso.where(:ativo => true, :empresa_id => session[:empresa_id])
  end

  # GET /permissoes_acessos/1
  # GET /permissoes_acessos/1.json
  def show
  end

  # GET /permissoes_acessos/new
  def new
    @permissoes_acesso = PermissoesAcesso.new
  end

  # GET /permissoes_acessos/1/edit
  def edit
  end

  # POST /permissoes_acessos
  # POST /permissoes_acessos.json
  def create
    @permissoes_acesso = PermissoesAcesso.new(permissoes_acesso_params)
    @permissoes_acesso.ativo = true
    @permissoes_acesso.empresa_id = session[:empresa_id]

    respond_to do |format|
      if @permissoes_acesso.save
        format.html { redirect_to @permissoes_acesso, notice: 'Permissões de Acesso cadastrado com sucesso.' }
        format.json { render action: 'show', status: :created, location: @permissoes_acesso }
      else
        format.html { render action: 'new' }
        format.json { render json: @permissoes_acesso.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /permissoes_acessos/1
  # PATCH/PUT /permissoes_acessos/1.json
  def update
    @permissoes_acesso.empresa_id = session[:empresa_id]
    
    respond_to do |format|
      @permissoes_acesso.update(empresa_id: session[:empresa_id])
      if @permissoes_acesso.update(permissoes_acesso_params)
        format.html { redirect_to @permissoes_acesso, notice: 'Permissões de Acesso atualizado com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @permissoes_acesso.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /permissoes_acessos/1
  # DELETE /permissoes_acessos/1.json
  def destroy
    @permissoes_acesso.update(ativo: :false)
    respond_to do |format|
      format.html { redirect_to permissoes_acessos_url, notice: 'Permissões de Acesso deletado com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_permissoes_acesso
      @permissoes_acesso = PermissoesAcesso.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def permissoes_acesso_params
      params.require(:permissoes_acesso).permit(:usuario_id, :modulo_id, :ativo, :empresa_id)
    end
end
