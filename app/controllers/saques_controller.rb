class SaquesController < ApplicationController
  before_action :set_saque, only: [:show, :edit, :update, :destroy]

  # GET /saques
  # GET /saques.json
  def index
    @saques = Saque.joins(:conta).where("contas.pessoa_id = ?", session[:pessoa]).where(ativo: true, empresa_id: session[:empresa_id])
  end

  # GET /saques/1
  # GET /saques/1.json
  def show
  end

  # GET /saques/new
  def new
    @saque = Saque.new
    @saldo_conta = Conta.where(ativo: true, empresa_id: session[:empresa_id]).first.saldo
  end

  # GET /saques/1/edit
  def edit
  end

  # POST /saques
  # POST /saques.json
  def create
    @saque = Saque.new(saque_params)
    @saque.ativo = true
    @saque.empresa_id = session[:empresa_id]
    senha_digitada = params[:senha]

    depositos = Deposito.where(ativo: true, empresa_id: session[:empresa_id]).sum(:valor_deposito)
    transferencias = Transferencia.where(ativo: true, empresa_id: session[:empresa_id]).sum(:valor_transferencia)
    transferencias_totais = Transferencia.where(ativo: true, empresa_id: session[:empresa_id]).sum(:taxa_transferencia)
    saques = Saque.where(ativo: true, empresa_id: session[:empresa_id]).sum(:valor_saque)
    saldo = depositos - transferencias - saques - transferencias_totais

    if ((saldo - @saque.valor_saque).to_f > 0)
      calculo_saldo = true
    end

    if (senha_digitada == @saque.conta.senha && saldo != 0 && calculo_saldo == true)
      @liberacao = true
      if @saque.save
        @id_conta = @saque.id
      end
    else
      @liberacao = false
    end
    
    render :new
  end

  # PATCH/PUT /saques/1
  # PATCH/PUT /saques/1.json
  def update
    respond_to do |format|
      if @saque.update(saque_params)
        format.html { redirect_to @saque, notice: 'Atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @saque }
      else
        format.html { render :edit }
        format.json { render json: @saque.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /saques/1
  # DELETE /saques/1.json
  def destroy
    @saque.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to saques_url, notice: 'Deletado com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_saque
      @saque = Saque.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def saque_params
      params.require(:saque).permit(:conta_id, :valor_saque, :empresa_id, :ativo)
    end
end
