class TransferenciasController < ApplicationController
  before_action :set_transferencia, only: [:show, :edit, :update, :destroy]

  # GET /transferencias
  # GET /transferencias.json
  def index
    pessoa
    @transferencias = Transferencia.joins(:conta).where("contas.pessoa_id = ?", session[:pessoa]).where(ativo: true, empresa_id: session[:empresa_id])
  end

  # GET /transferencias/1
  # GET /transferencias/1.json
  def show
  end

  # GET /transferencias/new
  def new
    @transferencia = Transferencia.new
    hora_atual = Time.now.strftime("%H").to_i
    if (Date.today.wday > 0 && Date.today.wday < 6 && hora_atual > 9 && hora_atual < 19)
      @taxa_transferencia = 5
    else
      @taxa_transferencia = 7
    end
  end

  # GET /transferencias/1/edit
  def edit
  end

  # POST /transferencias
  # POST /transferencias.json
  def create
    @transferencia = Transferencia.new(transferencia_params)
    @transferencia.ativo = true
    @transferencia.empresa_id = session[:empresa_id]

    senha_digitada = params[:senha]

    depositos = Deposito.where(ativo: true, empresa_id: session[:empresa_id]).sum(:valor_deposito)
    transferencias = Transferencia.where(ativo: true, empresa_id: session[:empresa_id]).sum(:valor_transferencia)
    transferencias_totais = Transferencia.where(ativo: true, empresa_id: session[:empresa_id]).sum(:taxa_transferencia)
    saques = Saque.where(ativo: true, empresa_id: session[:empresa_id]).sum(:valor_saque)
    saldo = depositos - transferencias - saques

    if ((saldo - @transferencia.valor_transferencia).to_f > 0)
      calculo_saldo = true
    end

    if (senha_digitada == Conta.find(@transferencia.conta_origem).senha && saldo != 0 && calculo_saldo == true)
      @liberacao = true
      
      @transferencia.taxa_transferencia = transferencia_params[:taxa_transferencia].gsub("R$ ", "").to_f
      
      if @transferencia.save
        @id_conta = @transferencia.id
      end
    else
      @liberacao = false
    end

    hora_atual = Time.now.strftime("%H").to_i
    if (Date.today.wday > 0 && Date.today.wday < 6 && hora_atual > 9 && hora_atual < 19)
      @taxa_transferencia = 5
    else
      @taxa_transferencia = 7
    end
    
    render :new
  end

  # PATCH/PUT /transferencias/1
  # PATCH/PUT /transferencias/1.json
  def update
    respond_to do |format|
      if @transferencia.update(transferencia_params)
        format.html { redirect_to @transferencia, notice: 'Atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @transferencia }
      else
        format.html { render :edit }
        format.json { render json: @transferencia.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transferencias/1
  # DELETE /transferencias/1.json
  def destroy
    @transferencia.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to transferencias_url, notice: 'Deletado com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transferencia
      @transferencia = Transferencia.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def transferencia_params
      params.require(:transferencia).permit(:conta_origem, :conta_destino, :valor_transferencia, :data_transferencia, :taxa_transferencia, :tipo_transferencia, :empresa_id, :ativo)
    end
end
