class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_usuario!
  layout :layout_by_resource
  before_action :configure_permitted_parameters, if: :devise_controller?


  def layout_by_resource
    if devise_controller?
      "devise"
    else
      "application"
    end
  end

  def after_sign_in_path_for(resource)
    aceite_termo_usuario = !current_usuario.nil? ? AceiteTermo.where(ativo: true, usuario_id: current_usuario.id).take : nil
    usuario = current_usuario.pessoa.id
   
    if !current_usuario.aceite_termo.nil? && !current_usuario.pessoa.nil?
      listagem_empresas_path
      @nome_empresa = !Empresa.where(:ativo => true).nil? ? Empresa.where(:ativo => true).first : nil
      session[:empresa_id] = !@nome_empresa.nil? ? @nome_empresa.id : nil
      session[:telefone] = !@nome_empresa.nil? ? @nome_empresa.telefone : nil
      session[:pessoa] = !usuario.nil? ? usuario : nil
    elsif aceite_termo_usuario.nil?
      @nome_empresa = !Empresa.where(:ativo => true).nil? ? Empresa.where(:ativo => true).first : nil
      session[:empresa_id] = !@nome_empresa.nil? ? @nome_empresa.id : nil
      session[:telefone] = !@nome_empresa.nil? ? @nome_empresa.telefone : nil
      session[:pessoa] = !usuario.nil? ? usuario : nil
      leitura_termos_aceite_termos_path
    else
      @nome_empresa = !Empresa.where(:ativo => true).nil? ? Empresa.where(:ativo => true).first : nil
      session[:empresa_id] = !@nome_empresa.nil? ? @nome_empresa.id : nil
      session[:telefone] = !@nome_empresa.nil? ? @nome_empresa.telefone : nil
      session[:pessoa] = !usuario.nil? ? usuario : nil
      @noticia = "logou"
      root_path
    end
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:nome, :sobrenome])
  end

end
