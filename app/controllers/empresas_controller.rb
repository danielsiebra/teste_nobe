class EmpresasController < ApplicationController
  before_action :set_empresa, only: [:show, :edit, :update, :destroy]

  # GET /empresas
  # GET /empresas.json
  def index
    @empresas = Empresa.where(ativo: true).order(:nome)
  end

  # GET /empresas/1
  # GET /empresas/1.json
  def show
  end

  # GET /empresas/new
  def new
    @empresa = Empresa.new
  end

  # GET /empresas/1/edit
  def edit
  end

  # POST /empresas
  # POST /empresas.json
  def create
    @empresa = Empresa.new(empresa_params)

    respond_to do |format|
      if @empresa.save
        session[:empresa_id] = @empresa.id
        @modulo_id = !Modulo.find_by(:descricao => "Administrador").nil? ? Modulo.find_by(:descricao => "Administrador").id : nil
        if @modulo_id.nil?
          Modulo.create(:ativo => true, :empresa_id => @empresa.id, :descricao => "Administrador")
          @modulo_id = !Modulo.find_by(:descricao => "Administrador").nil? ? Modulo.find_by(:descricao => "Administrador").id : nil
          @permissoes = PermissoesAcesso.create(:ativo => true, :empresa_id => @empresa.id, :usuario_id => current_usuario.id, :modulo_id => @modulo_id)
        else
          @permissoes = PermissoesAcesso.create(:ativo => true, :empresa_id => @empresa.id, :usuario_id => current_usuario.id, :modulo_id => @modulo_id)
        end
        format.html { redirect_to @empresa, notice: 'Empresa cadastrada com sucesso.' }
        format.json { render action: 'show', status: :created, location: @empresa }
      else
        format.html { render action: 'new' }
        format.json { render json: @empresa.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /empresas/1
  # PATCH/PUT /empresas/1.json
  def update
    respond_to do |format|
      if @empresa.update(empresa_params)
        format.html { redirect_to @empresa, notice: 'Empresa atualizado com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @empresa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /empresas/1
  # DELETE /empresas/1.json
  def destroy
    @empresa.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to empresas_url, notice: 'Empresa deletado com sucesso.' }
      format.json { head :no_content }
    end
  end

  def setar
    session[:empresa_id] = params[:empresa_id]
    redirect_to dashboards_dashboard_1_path
  end

  def adicionar_participante
    @permissoes = PermissoesAcesso.create(:ativo => true, :empresa_id => params[:adicionar_participante][:empresa_id], :usuario_id => params[:adicionar_participante][:usuario_id], :modulo_id => params[:adicionar_participante][:modulo_id])
    redirect_to dashboards_dashboard_1_path 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_empresa
      @empresa = Empresa.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def empresa_params
      params.require(:empresa).permit(:nome, :razao_social, :cnpj, :telefone, :email, :cep, :endereco, :numero, :complemento, :ativo)
    end
end
