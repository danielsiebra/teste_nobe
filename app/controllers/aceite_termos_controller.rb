class AceiteTermosController < ApplicationController
	def leitura_termos
		@aceite = AceiteTermo.find_by(usuario_id: current_usuario).nil? ? nil : AceiteTermo.find_by(usuario_id: current_usuario).id
		@pessoa_ativa = Pessoa.find_by(usuario_id: current_usuario).nil? ? nil : Pessoa.find_by(usuario_id: current_usuario).id
		if !@aceite.nil?
			aceite_novo = AceiteTermo.find_by(usuario_id: current_usuario, empresa_id: session[:empresa_id]).nil? ? nil : AceiteTermo.find_by(usuario_id: current_usuario, empresa_id: session[:empresa_id]).id
			if aceite_novo.nil?
				termos = TermosUso.find_by(id: @aceite.termos_uso_id)
				@aceite.update(empresa_id: session[:empresa_id])
				termos.update(empresa_id: session[:empresa_id])
			end
			if !@pessoa_ativa.nil?
				pessoa_nova = Pessoa.find_by(usuario_id: current_usuario, empresa_id: session[:empresa_id]).nil? ? nil : Pessoa.find_by(usuario_id: current_usuario, empresa_id: session[:empresa_id]).id
				if pessoa_nova.nil?
					@pessoa_ativa.update(empresa_id: session[:empresa_id])
				end
				redirect_to root_path, notice: 'Login efetuado com sucesso.'
			else
				redirect_to new_pessoa_path, notice: 'Finalize o cadastro de pessoa.'
			end
		else
			empresa = session[:empresa_id]
			session[:pessoa] = !@pessoa_ativa.nil? ? @pessoa_ativa : nil
			@termo = TermosUso.where(:empresa_id => empresa, ativo: true)
	    	render :layout => "empty"
	    end
	end

	def aceitar_termos
		begin
			ActiveRecord::Base.transaction do
				AceiteTermo.new(usuario_id: current_usuario.id, termos_uso_id: params[:termos_uso_id], data_aceite: Time.now, empresa_id: session[:empresa_id]).save
				redirect_to new_pessoa_path
			end
	  	rescue
	  		redirect_to leitura_termos_aceite_termos_path, notice: 'Erro ao aceitar termos de uso, favor comunicar ao suporte o imprevisto!'
	  	end
	end

	def cadastro_pessoa
		@pessoa = Pessoa.new
	end
end
