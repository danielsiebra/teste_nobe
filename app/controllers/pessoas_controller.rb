class PessoasController < ApplicationController
  before_action :set_pessoa, only: [:show, :edit, :update, :destroy]

  # GET /pessoas
  # GET /pessoas.json
  def index
    @pessoas = Pessoa.where(:ativo => true, :empresa_id => session[:empresa_id]).order(:nome)
  end

  # GET /pessoas/1
  # GET /pessoas/1.json
  def show
  end

  # GET /pessoas/new
  def new
    @pessoa = Pessoa.new
  end

  # GET /pessoas/1/edit
  def edit
  end

  # POST /pessoas
  # POST /pessoas.json
  def create
    @pessoa = Pessoa.new(pessoa_params)
    session[:empresa_id] = 1
    @pessoa.usuario_id = current_usuario.id
    @pessoa.cpf = params[:pessoa][:cpf].gsub(".", "").gsub("-", "")
    @pessoa.cep = params[:pessoa][:cep].gsub("-", "")
    @pessoa.empresa_id = session[:empresa_id]
    @pessoa.ativo = true

    respond_to do |format|
      if @pessoa.save
        format.html { redirect_to root_path, notice: 'Cadastro realizado com sucesso.' }
      else
        format.html { render action: 'new' }
        format.json { render json: @pessoa.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pessoas/1
  # PATCH/PUT /pessoas/1.json
  def update
    respond_to do |format|
      if @pessoa.update(pessoa_params)
        format.html { redirect_to @pessoa, notice: 'Cadastro atualizado com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @pessoa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pessoas/1
  # DELETE /pessoas/1.json
  def destroy
    @pessoa.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to pessoas_url, notice: 'Cadastro deletado com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pessoa
      @pessoa = Pessoa.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pessoa_params
      params.require(:pessoa).permit(:tipo_pessoa, :nome, :sobrenome, :data_nascimento, :genero, :usuario_id, :ativo, :empresa_id, :telefone, :rua, :cidade, :bairro, :numero, :complemento, :estado, :cpf, :rg)
    end
end
