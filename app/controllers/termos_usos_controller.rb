class TermosUsosController < ApplicationController
  before_action :set_termos_uso, only: [:show, :edit, :update, :destroy]

  # GET /termos_usos
  # GET /termos_usos.json
  def index
    @termos_usos = TermosUso.where(:ativo => true, empresa_id: session[:empresa_id]).order(:inicio_virgencia)
  end

  # GET /termos_usos/1
  # GET /termos_usos/1.json
  def show
  end

  # GET /termos_usos/new
  def new
    @termos_uso = TermosUso.new
  end

  # GET /termos_usos/1/edit
  def edit
  end

  # POST /termos_usos
  # POST /termos_usos.json
  def create
    @termos_uso = TermosUso.new(termos_uso_params)
    @termos_uso.ativo = true
    if !session[:empresa_id].nil?
      @termos_uso.empresa_id = session[:empresa_id]
    else
      @termos_uso.empresa_id = !Empresa.where(ativo: true).nil? ? Empresa.where(ativo: true).first.id : nil
    end

    respond_to do |format|
      if @termos_uso.save
        format.html { redirect_to @termos_uso, notice: 'Termos cadastrado com sucesso.' }
        format.json { render action: 'show', status: :created, location: @termos_uso }
      else
        format.html { render action: 'new' }
        format.json { render json: @termos_uso.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /termos_usos/1
  # PATCH/PUT /termos_usos/1.json
  def update
    respond_to do |format|
      if @termos_uso.update(termos_uso_params)
        @termos_uso.update(ativo: true)
        @termos_uso.update(empresa_id: session[:empresa_id])
        format.html { redirect_to @termos_uso, notice: 'Termos atualizado com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @termos_uso.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /termos_usos/1
  # DELETE /termos_usos/1.json
  def destroy
    @termos_uso.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to termos_usos_url, notice: 'Termos deletado com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_termos_uso
      @termos_uso = TermosUso.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def termos_uso_params
      params.require(:termos_uso).permit(:inicio_virgencia, :termino_virgencia, :termos, :ativo, :empresa_id)
    end
end
