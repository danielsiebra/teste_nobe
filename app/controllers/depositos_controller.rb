class DepositosController < ApplicationController
  before_action :set_deposito, only: [:show, :edit, :update, :destroy]

  # GET /depositos
  # GET /depositos.json
  def index
    @depositos = Deposito.joins(:conta).where("contas.pessoa_id = ?", session[:pessoa]).where(ativo: true, empresa_id: session[:empresa_id])
  end

  # GET /depositos/1
  # GET /depositos/1.json
  def show
  end

  # GET /depositos/new
  def new
    @deposito = Deposito.new
  end

  # GET /depositos/1/edit
  def edit
  end

  # POST /depositos
  # POST /depositos.json
  def create
    @deposito = Deposito.new(deposito_params)
    @deposito.ativo = true
    @deposito.empresa_id = session[:empresa_id]

    respond_to do |format|
      if @deposito.save
        format.html { redirect_to @deposito, notice: 'Realizado com sucesso.' }
        format.json { render :show, status: :created, location: @deposito }
      else
        format.html { render :new }
        format.json { render json: @deposito.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /depositos/1
  # PATCH/PUT /depositos/1.json
  def update
    respond_to do |format|
      if @deposito.update(deposito_params)
        format.html { redirect_to @deposito, notice: 'Atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @deposito }
      else
        format.html { render :edit }
        format.json { render json: @deposito.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /depositos/1
  # DELETE /depositos/1.json
  def destroy
    @deposito.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to depositos_url, notice: 'Deletado com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_deposito
      @deposito = Deposito.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def deposito_params
      params.require(:deposito).permit(:conta_id, :valor_deposito, :tipo_deposito, :empresa_id, :ativo)
    end
end
