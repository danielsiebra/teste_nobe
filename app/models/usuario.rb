class Usuario < ApplicationRecord
	has_one :pessoa
	belongs_to :aceite_termo
	devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable
end
