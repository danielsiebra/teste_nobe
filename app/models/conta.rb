class Conta < ApplicationRecord
  belongs_to :pessoa
  belongs_to :empresa
  has_many :depositos
  has_many :saques
  has_many :transferencias
end
