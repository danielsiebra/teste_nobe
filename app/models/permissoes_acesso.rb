class PermissoesAcesso < ApplicationRecord
  belongs_to :usuario
  belongs_to :modulo
  belongs_to :empresa
end
