// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
//
//= require jquery.js
//= require rails-ujs
//= require jquery-ui.js
//= require bootstrap-sprockets
//= import Swal from 'sweetalert2/dist/sweetalert2.js'
//= require metisMenu/jquery.metisMenu.js
//= require pace/pace.min.js
//= require peity/jquery.peity.min.js
//= require slimscroll/jquery.slimscroll.min.js
//= require inspinia.js
//= require maskedinput
//= require chartkick
//= require Chart.bundle
Chartkick.configure({language: "pt-BR"});
//= require activestorage
//= require turbolinks
//= require_tree .
//= require serviceworker-companion

$(document).ready(function(){
    $(".opcao-fab").hide();
    $(".fab .main").click(function(){
      	var clicou = $(".fab").hasClass("show");
      	if (clicou == false) {
        	$(".opcao-fab").show();
        	$("#barra_menu").attr('class', '');
        	$(".fab").attr('class', 'fab show');
      	}
      	else {
        	$("#barra_menu").attr('class', 'fa fa-shopping-cart');
        	$(".fab").attr('class', 'fab');
    		$(".opcao-fab").hide();
      	}
    });

    $(".fab ul button li").click(function(){
      	$(".fab").toggle();
    });
});
