Myapp::Application.routes.draw do

  resources :transferencias
  resources :saques
  resources :depositos
  resources :contas
  resources :parcelas_emprestimos do
    collection do
      match 'tarefas', via: [:get]
      match 'pagamentos_feitos', via: [:get]
      match 'detalhes', via: [:get]
      match 'pagar', via: [:get]
    end
  end
  resources :emprestimos
  resources :autorizacao_pagamentos
  resources :enderecos_empresas
  resources :filiais_empresas
  resources :enderecos_clientes
  resources :clientes do
    collection do
      match 'campo_cliente', via: [:get]
    end
  end
  resources :formas_pagamentos
  namespace :checkout do
    resources :payments, only: [:create]
    resources :notifications, only: [:create]
  end
  resources :aceite_termos do
    collection do
      match 'leitura_termos', via: [:get]
      match 'aceitar_termos', via: [:get]
      match 'cadastro_pessoa', via: [:get]
    end
  end
  resources :landing do
    collection do
      match 'compra_aprovada', via: [:get, :post]
      match 'compra_recusada', via: [:get, :post]
      match 'compra_pendente', via: [:get, :post]

      match 'mercado_pago', via: [:get, :post]

      match 'observacao', via: [:get, :post]
    end 
  end
  resources :termos_usos
  resources :pessoas

  resources :permissoes_acessos
  resources :modulos
  
  resources :empresas do
    collection do
      get 'listagem'
      get 'setar'
      get 'informacoes'
      post 'adicionar_participante'
    end
  end

  devise_for :usuarios
  
  # You can have the root of your site routed with "root"
  root to: 'dashboards#dashboard_1'
  
  get 'dashboards/dashboard_1'

  get 'dashboards/filtrar'
  post 'dashboards/filtrar'
  post 'landing/logar'
end
